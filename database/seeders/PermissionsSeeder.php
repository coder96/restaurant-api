<?php

namespace Database\Seeders;

use App\Dictionaries\PermissionsDictionary;
use App\Dictionaries\RoleDictionary;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsSeeder extends Seeder
{
    private $roles = [
        [
            'guard_name' => 'web',
            'name' => RoleDictionary::SUPER_ADMIN,
        ],
        [
            'guard_name' => 'web',
            'name' => RoleDictionary::ADMIN,
        ],
        [
            'guard_name' => 'web',
            'name' => RoleDictionary::OWNER,
        ],
    ];

    private $permissions = [
        [
            'guard_name' => 'web',
            'name' => PermissionsDictionary::CREATE_FOOD,
        ],
        [
            'guard_name' => 'web',
            'name' => PermissionsDictionary::CREATE_CATEGORY,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert($this->roles);
        Permission::insert($this->permissions);

        $users = User::all()->where('id', '>', 2);
        foreach ($users as $key => $user) {
            $user->assignRole(RoleDictionary::OWNER);
        }

        User::find(1)->assignRole(RoleDictionary::SUPER_ADMIN);
        User::find(2)->assignRole(RoleDictionary::ADMIN);
    }
}
