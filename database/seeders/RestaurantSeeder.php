<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Meal;
use App\Models\MealPrice;
use App\Models\Restaurant;
use Illuminate\Database\Seeder;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Restaurant::factory()
            ->has(
                Category::factory()
                    ->has(
                        Meal::factory()
                            ->has(MealPrice::factory()->count(rand(1, 3)))
                            ->count(rand(5, 10))
                            ->state(function (array $attributes, Category $category) {
                                return ['user_id' => $category->restaurant->user_id];
                            })
                    )
                    ->count(rand(5, 12))
            )
            ->count(10)
            ->create();
    }
}
