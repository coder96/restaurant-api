<?php

namespace Database\Seeders;

use App\Models\Restaurant;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->runForAdmin();

        User::factory(10)->create();
        $this->call([
            PermissionsSeeder::class,
            RestaurantSeeder::class,
        ]);

        $restaurant = Restaurant::find(1);
        $restaurant->slug = 'my-restaurant';
        $restaurant->save();
    }

    private function runForAdmin()
    {
        User::insert([
            [
                'name' => 'super_admin',
                'email' => 'super_admin@example.com',
                'password' => bcrypt('admin')
            ],
            [
                'name' => 'admin',
                'email' => 'admin@example.com',
                'password' => bcrypt('admin')
            ],
            [
                'name' => 'demo',
                'email' => 'demo@example.com',
                'password' => bcrypt('demo')
            ]
        ]);
    }
}
