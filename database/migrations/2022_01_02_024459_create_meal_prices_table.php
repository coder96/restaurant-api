<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMealPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_prices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('meal_id');
            $table->decimal('quantity', 8, 2, true);
            $table->enum('measure', ['gram', 'pieces', 'portion']);
            $table->decimal('price', 8, 2, true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_prices');
    }
}
