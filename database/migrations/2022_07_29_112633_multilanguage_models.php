<?php

use App\Models\Category;
use App\Models\Meal;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MultiLanguageModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translations', function (Blueprint $table) {
            $table->id();
            $table->string('model');
            $table->integer('model_id')->unsigned();
            $table->string('locale', 2);
            $table->string('field_name');
            $table->string('value');

            $table->unique([
                'model',
                'model_id',
                'locale',
                'field_name',
            ]);
        });

        $meals = Meal::all();
        foreach ($meals as $meal) {
            $meal->addTranslation('ru', 'name', $meal->name);
            $meal->addTranslation('ru', 'description', $meal->description);
        }

        Schema::table('meals', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('description');
        });

        $categories = Category::all();
        foreach ($categories as $category) {
            $category->addTranslation('ru', 'name', $category->name);
        }

        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translations');
    }
}
