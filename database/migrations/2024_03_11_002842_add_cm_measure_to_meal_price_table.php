<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("alter table meal_prices
    modify measure enum ('gram', 'pieces', 'portion', '', 'ml', 'liter', 'cm') not null");
        DB::statement('UPDATE meal_prices SET measure = "cm" WHERE measure = "ml"');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
