<?php

namespace Database\Factories;

use App\Models\Restaurant;
use Illuminate\Database\Eloquent\Factories\Factory;

class RestaurantFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Restaurant::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = $this->faker;

        return [
            'user_id' => $faker->unique()->numberBetween(1, 10),
            'name' => $faker->company,
            'description' => $faker->text(),
            'address' => $faker->address,
            'phone' => $faker->phoneNumber,
            'social_networks' => [
                'whatsapp' => $faker->boolean,
                'telegram' => $faker->boolean,
                'facebook' => 'https://www.facebook.com/profile.php?id=100024346160190',
                'instagram' => 'https://www.instagram.com/doniyor_valizhanov/',
            ],
            'slug' => $faker->unique()->slug,
        ];
    }
}
