<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Meal;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class MealFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Meal::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'user_id' => $this->faker->numberBetween(1, User::count()),
            'category_id' => $this->faker->numberBetween(1, Category::count()),
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function (Meal $meal) {
            foreach (config('locales') as $locale) {
                $faker = Faker::create($locale['code']);
                $meal->addTranslation($locale['name'], 'name', $faker->name);
                $meal->addTranslation($locale['name'], 'description', $faker->text);
            }
        });
    }
}
