<?php

namespace Database\Factories;

use App\Models\Category;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'restaurant_id' => $this->faker->numberBetween(1, 10),
            'icon' => 'breakfast',
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function (Category $category) {
            foreach (config('locales') as $locale) {
                $faker = Faker::create($locale['code']);
                $category->addTranslation($locale['name'], 'name', $faker->name);
            }
        });
    }
}
