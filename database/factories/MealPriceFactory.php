<?php

namespace Database\Factories;

use App\Models\Meal;
use App\Models\MealPrice;
use Illuminate\Database\Eloquent\Factories\Factory;

class MealPriceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MealPrice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'meal_id' => $this->faker->numberBetween(1, Meal::count()),
            'quantity' => $this->faker->numberBetween(1, 3),
            'measure' => $this->faker->randomElement([
                'gram',
                'pieces',
                'portion'
            ]),
            'price' => $this->faker->randomFloat(2, 50, 1000)
        ];
    }
}
