<?php

return [
    [
        'name' => 'ru',
        'title' => 'Русский',
        'code' => 'ru_RU',
    ],
    [
        'name' => 'en',
        'title' => 'English',
        'code' => 'en',
    ],
];
