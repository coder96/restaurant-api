<?php

return [
    'main_size' => env('RESTAURANT_MAIN_IMAGE_SIZE', '900'),
    'thumbnail_size' => env('RESTAURANT_THUMB_IMAGE_SIZE', '420'),
    'upload_path' => 'restaurants/'
];
