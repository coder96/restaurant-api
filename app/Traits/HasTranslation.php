<?php

namespace App\Traits;

use App\Exceptions\BadRequestException;
use App\Models\Translation;
use Arr;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait HasTranslation
{
    public function locales(): HasMany
    {
        return $this->hasMany(Translation::class, 'model_id')
            ->where('model', $this->getModelClass());
    }

    /**
     * @param string $locale
     * @param string $field
     * @param string $value
     * @throws \App\Exceptions\BadRequestException
     * @return bool
     */
    public function addTranslation(string $locale, string $field, string $value)
    {
        if (!in_array($locale, Arr::pluck(config('locales'), 'name'))) {
            throw new BadRequestException('Not allowed locale: ' . $locale);
        }
        if (!in_array($field, $this->translatableFields ?? [])) {
            throw new BadRequestException('Not allowed to translate field: ' . $field);
        }

        return Translation::updateOrInsert([
            'locale' => $locale,
            'model' => $this->getModelClass(),
            'model_id' => $this->id,
            'field_name' => $field,
        ], [
            'value' => $value,
        ]);
    }

    /**
     * @param string|null $locale
     * @param string|null $field
     * @return bool
     */
    public function deleteTranslation(string $locale = null, string $field = null)
    {
        $query = Translation::query()
            ->where([
                'model' => $this->getModelClass(),
                'model_id' => $this->id,
            ]);

        if ($locale) {
            $query->where('locale', $locale);
        }

        if ($field) {
            $query->where('field_name', $field);
        }

        return $query->delete();
    }

    /**
     * @return array|array[]
     */
    public function getLocales(): array
    {
        $locales = array_map(fn ($locales) => Arr::pluck($locales, 'value', 'field_name'), $this->locales->groupBy('locale')->toArray());

        foreach (config('locales') as $locale) {
            foreach ($this->translatableFields as $field) {
                if (!isset($locales[$locale['name']][$field])) {
                    $locales[$locale['name']][$field] = '';
                }
            }
        }

        return $locales;
    }

    private function getModelClass(): string
    {
        return static::class;
    }
}
