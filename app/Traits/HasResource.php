<?php

namespace App\Traits;

use App\Models\Resource;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property-read string $photo
 * @property-read string $thumbnail
 */
trait HasResource
{
    public function resource(): HasOne
    {
        return $this->hasOne(Resource::class, 'model_id')
            ->where('model_type', static::class);
    }

    public function getPhotoAttribute(): string
    {
        return $this->resource ? $this->resource->url() : '';
    }

    public function getPhotoBlobAttribute(): string
    {
        return $this->resource ? $this->resource->photoBlob() : '';
    }

    public function getThumbnailAttribute(): string
    {
        return $this->resource ? $this->resource->thumbnailUrl() : '';
    }
}
