<?php

namespace App\Traits;

use App\Models\Resource;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;

/**
 * @property-read Collection $resources
 * @property-read string $thumbnail
 */
trait HasResources
{
    public function resources(): HasMany
    {
        return $this->hasMany(Resource::class, 'model_id')
            ->where('model_type', static::class);
    }

    public function getPhotosAttribute(): array
    {
        return $this->resources->map(fn ($item) => [
            'id' => $item->id,
            'url' => $item->url(),
            'thumbnail' => $item->thumbnailUrl(),
        ])->toArray();
    }
}
