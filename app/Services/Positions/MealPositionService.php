<?php

namespace App\Services\Positions;

use App\Services\MyRestaurantService;
use Illuminate\Database\Eloquent\Collection;

class MealPositionService extends BasePositionService
{
    /**
     * @var int Category id
     */
    private int $categoryId;

    /**
     * @param MyRestaurantService $restaurantService
     * @param int $categoryId
     */
    public function __construct(MyRestaurantService $restaurantService, int $categoryId)
    {
        $this->categoryId = $categoryId;
        parent::__construct($restaurantService);
    }

    /**
     * @inheritdoc
     */
    protected function getItems(): Collection
    {
        return $this->restaurantService
            ->meals()
            ->where('category_id', $this->categoryId)
            ->get();
    }
}
