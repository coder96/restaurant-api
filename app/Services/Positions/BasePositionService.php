<?php

namespace App\Services\Positions;


use App\Services\MyRestaurantService;
use Illuminate\Database\Eloquent\Collection;

abstract class BasePositionService
{
    protected MyRestaurantService $restaurantService;

    /**
     * @param MyRestaurantService $restaurantService
     */
    public function __construct(MyRestaurantService $restaurantService)
    {
        $this->restaurantService = $restaurantService;
    }

    /**
     * @param array $positions
     * @return bool
     */
    public function updatePositions(array $positions): bool
    {
        $positions = $this->preparePositions($positions);
        $collection = $this->getItems();

        foreach ($collection as $item) {
            $item->position = $positions[$item->id];
            $item->save();
        }

        return true;
    }

    /**
     * @param array $array
     * @return array
     */
    protected function preparePositions(array $array): array
    {
        $result = [];
        foreach ($array as $item) {
            $result[$item['id']] = $item['position'];
        }

        return $result;
    }

    /**
     * @return Collection
     */
    protected abstract function getItems(): Collection;
}
