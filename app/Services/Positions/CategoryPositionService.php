<?php

namespace App\Services\Positions;

use Illuminate\Database\Eloquent\Collection;

class CategoryPositionService extends BasePositionService
{
    /**
     * @inheritdoc
     */
    protected function getItems(): Collection
    {
        return $this->restaurantService->categories();
    }
}
