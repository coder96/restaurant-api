<?php

namespace App\Services;

use App\Models\Meal;

class MealService
{
    /**
     * @param Meal $meal
     * @return bool
     */
    public function activate(Meal $meal): bool
    {
        $meal->is_active = 1;
        return $meal->save();
    }

    /**
     * @param Meal $meal
     * @return bool
     */
    public function deactivate(Meal $meal): bool
    {
        $meal->is_active = 0;
        return $meal->save();
    }
}
