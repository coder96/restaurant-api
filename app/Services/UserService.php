<?php

namespace App\Services;

use App\Dictionaries\RoleDictionary;
use App\Exceptions\ApiError;
use App\Http\Requests\UserPostRequest;
use App\Models\Restaurant;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UserService
{
    public function store(UserPostRequest $request)
    {
        try {
            app('DB')::beginTransaction();
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);
            $role = Role::findByName(RoleDictionary::OWNER, 'web');
            $user->assignRole($role);

            Restaurant::create([
                'user_id' => $user->id,
                'name' => $request->restaurant_name,
                'slug' => $request->restaurant_slug,
                'phone' => $request->phone,
            ]);
            app('DB')::commit();

            return $user;
        } catch (\Exception $exception) {
            app('DB')::rollBack();
            throw new ApiError('Insert operation failed', 500, $exception);
        }
    }
}
