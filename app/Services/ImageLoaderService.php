<?php

namespace App\Services;

use App\Exceptions\ApiError;
use App\Models\Meal;
use App\Models\Resource;
use App\Repositories\ResourceRepository;
use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Imagick;
use ImagickException;
use ImagickPixel;

class ImageLoaderService
{
    private Imagick $imagick;
    private Storage $storage;

    /**
     * @var string Image name
     */
    private string $name = '';

    /**
     * @var string Loaded image extension
     */
    private string $ext = '';

    /**
     * @var string Path to upload
     */
    private string $uploadPath = '';

    private int $width = 0;
    private int $height = 0;

    public function __construct(Imagick $imagick, Storage $storage)
    {
        $this->imagick = $imagick;
        $this->storage = $storage;
    }

    /**
     * @throws ImagickException
     * @throws Exception
     */
    public function upload(string $modelClass, int $modelId, string $content)
    {
        $this->loadImage($this->parseFile($content));

        $resource = app(ResourceRepository::class)->create([
            'user_id' => app(MyRestaurantService::class)->user()->id,
            'model_type' => $modelClass,
            'model_id' => $modelId,
            'name' => Str::uuid(),
            'extension' => $this->ext,
            'path' => $this->getPath($modelClass),
        ]);
        $this->name = $resource->name;
        $this->uploadPath = $this->checkPath($resource->path);

        $this->createMain();
        $this->createThumbnail();
    }

    public function unlink(Resource $resource)
    {
        $this->name = $resource->name;
        $this->ext = $resource->extension;

        if ($this->storage::delete($resource->path . $this->getResultFileName()) &&
            $this->storage::delete($resource->path . $this->getResultFileName('_thumb'))) {
            $resource->delete();
        } else {
            throw new ApiError('Image cannot be deleted');
        }
    }

    private function checkPath(string $path): string
    {
        if (!$this->storage::exists($path)) {
            $this->storage::makeDirectory($path);
        }

        return $this->storage::path($path);
    }

    /**
     * @throws ImagickException
     * @throws Exception
     */
    private function loadImage(string $content)
    {
        $this->imagick->readImageBlob(base64_decode($content));

        $geometry = $this->imagick->getImageGeometry();
        $this->width = $geometry['width'];
        $this->height = $geometry['height'];

        $this->setImageExtension($this->imagick->getImageMimeType());
    }

    private function parseFile(string $content): string
    {
        $arr = explode(',', $content);

        return $arr[1];
    }

    /**
     * @throws ImagickException
     */
    private function save($path, $imageSize, $quality = 80)
    {
        if (($this->width / $this->height) >= 1) {
            $newW = $imageSize;
            $newH = ceil($this->height * ($newW / $this->width));
        } else {
            $newH = $imageSize;
            $newW = ceil($this->width * ($newH / $this->height));
        }

        $temp = new Imagick();
        $temp->newImage($newW, $newH, 'white');
        $this->imagick->thumbnailImage($newW, $newH, true);
        $temp->compositeImage($this->imagick, imagick::COMPOSITE_OVER, 0, 0);

        $temp->setCompression(imagick::COMPRESSION_JPEG);
        $temp->setCompressionQuality($quality);
        $this->autoRotate($temp);

        $temp->setImageDepth(8);
        $temp->setImageFileName($path);
        $temp->setImageFormat('jpg');
        $temp->writeImage($path);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function createThumbnail(): string
    {
        $imageSize = $this->getConfig('thumbnail_size');
        $resultFilename = $this->getResultFileName('_thumb');
        $path = $this->uploadPath . $resultFilename;

        $this->save($path, $imageSize, 100);

        return $resultFilename;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function createMain(): string
    {
        $imageSize = $this->getConfig('main_size');
        $resultFilename = $this->getResultFileName();
        $path = $this->uploadPath . $resultFilename;

        $this->save($path, $imageSize, 100);

        return $resultFilename;
    }

    /**
     * @throws Exception
     */
    private function setImageExtension(string $mime)
    {
        switch ($mime) {
            case 'image/gif':
            case 'image/png':
            case 'image/x-png':
            case 'image/jpeg':
            case 'image/x-jpeg':
            case 'image/pjpeg':
                $this->ext = 'jpg';
                break;
            default:
                throw new Exception('Invalid image extension: ' . $mime, 3);
        }
    }

    /**
     * @return void
     * @throws ImagickException
     */
    private function autoRotate(Imagick $imagick)
    {
        switch($imagick->getImageOrientation()) {
            case imagick::ORIENTATION_TOPRIGHT:
                $imagick->flopImage();
                break;

            case imagick::ORIENTATION_BOTTOMRIGHT:
                $imagick->rotateimage(new ImagickPixel('none'), 180); // rotate 180 degrees
                break;

            case imagick::ORIENTATION_BOTTOMLEFT:
                $imagick->flopImage();
                $imagick->rotateImage(new ImagickPixel('none'), 180);
                break;

            case imagick::ORIENTATION_LEFTTOP:
                $imagick->flopImage();
                $imagick->rotateImage(new ImagickPixel('none'), -90);
                break;

            case imagick::ORIENTATION_RIGHTTOP:
                $imagick->rotateimage(new ImagickPixel('none'), 90); // rotate 90 degrees CW
                break;

            case imagick::ORIENTATION_RIGHTBOTTOM:
                $imagick->flopImage();
                $imagick->rotateImage(new ImagickPixel('none'), 90);
                break;

            case imagick::ORIENTATION_LEFTBOTTOM:
                $imagick->rotateimage(new ImagickPixel('none'), -90); // rotate 90 degrees CCW
                break;
            default:
                // DO NOTHING, THE IMAGE IS OK OR WE DON'T KNOW IF IT'S ROTATED
                break;
        }
    }

    private function getPath(string $model): string
    {
        $path = $this->getConfig('upload_path') . app(MyRestaurantService::class)->restaurant()->id . '/';
        if ($model == Meal::class) {
            $path .= 'meals/';
        }

        return $path;
    }

    private function getConfig(string $name)
    {
        return config('imageloader.' . $name);
    }

    /**
     * @param string $additional _thumb|_original - дополнение к имени файла
     * @return string
     */
    private function getResultFileName(string $additional = ''): string
    {
        return $this->name . $additional . '.' . $this->ext;
    }
}
