<?php

namespace App\Services;

use App\Http\Requests\RestaurantRequest;
use App\Models\Category;
use App\Models\Restaurant;
use App\Models\User;
use App\Repositories\RestaurantRepository;

class MyRestaurantService
{
    /**
     * @param array $params
     *
     * @throws \App\Exceptions\BadRequestException
     * @return Category
     */
    public function addCategory(array $params): Category
    {
        $params['restaurant_id'] = $this->restaurant()->id;

        $category = Category::create($params);

        foreach ($params['locales'] as $locale => $data) {
            foreach ($data as $field => $value) {
                if (!empty($value)) {
                    $category->addTranslation($locale, $field, $value);
                }
            }
        }

        return $category;
    }

    /**
     * @throws \App\Exceptions\BadRequestException
     */
    public function updateCategory(array $params, Category $category): Category
    {
        $category->update($params);

        foreach ($params['locales'] as $locale => $data) {
            foreach ($data as $field => $value) {
                if (!empty($value)) {
                    $category->addTranslation($locale, $field, $value);
                } else {
                    $category->deleteTranslation($locale, $field);
                }
            }
        }

        return $category;
    }

    /**
     * @return mixed
     */
    public function categories()
    {
        return $this->restaurant()->categories()->get();
    }

    /**
     * @return User
     */
    public function user()
    {
        return auth()->user();
    }

    /**
     * @return \App\Models\Restaurant|mixed|null
     */
    public function restaurant()
    {
        return $this->user()->restaurant;
    }

    /**
     * @return \App\Models\Restaurant|mixed|null
     */
    public function meals()
    {
        return $this->user()->meals();
    }

    /**
     * @throws \App\Exceptions\ApiError
     * @throws \ImagickException
     */
    public function update(Restaurant $restaurant, RestaurantRequest $request, ImageLoaderService $loaderService)
    {
        if ($request->new_photo) {
            $oldResource = $restaurant->resource;
            $loaderService->upload(Restaurant::class, $restaurant->id, $request->photo);
            if ($oldResource) {
                $loaderService->unlink($oldResource);
            }
        }

        app(RestaurantRepository::class)->update($request, $restaurant);

        return Restaurant::find($restaurant->id);
    }
}
