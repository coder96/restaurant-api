<?php

namespace App\Repositories;

use App\Http\Requests\RestaurantRequest;
use App\Models\Restaurant;

class RestaurantRepository
{
    public function create(RestaurantRequest $request)
    {
        return Restaurant::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'slug' => $request->slug,
        ]);
    }

    public function update(RestaurantRequest $request, Restaurant $restaurant)
    {
        return $restaurant->update([
            'name' => $request->name,
            'description' => $request->description,
            'address' => $request->address,
            'phone' => $request->phone,
            'social_networks' => $request->social_networks,
            'working_days' => $request->working_days,
            'working_time' => $request->working_time,
        ]);
    }
}
