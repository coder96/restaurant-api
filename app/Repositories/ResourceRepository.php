<?php

namespace App\Repositories;

use App\Models\Resource;

class ResourceRepository
{
    public function create(array $data): ?Resource
    {
        return Resource::create($data);
    }
}
