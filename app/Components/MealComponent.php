<?php

namespace App\Components;

use App\Http\Requests\MealRequest;
use App\Models\Meal;
use App\Services\ImageLoaderService;
use Illuminate\Support\Facades\DB;

class MealComponent
{
    /**
     * @var Meal
     */
    private $model;

    public function __construct(Meal $meal)
    {
        $this->model = $meal;
    }


    /**
     * @throws \ImagickException
     */
    public function store(MealRequest $request, int $userId, ImageLoaderService $loaderService)
    {
        $meal = $this->model;
        DB::transaction(function () use ($request, $userId, $loaderService, &$meal) {
            $meal = $this->model->create([
                'user_id' => $userId,
                'category_id' => $request->category_id,
                'youtube' => $request->youtube ?? null,
            ]);

            foreach ($request->prices as $price) {
                $meal->prices()->create($price);
            }

            foreach ($request->locales as $locale => $data) {
                foreach ($data as $field => $value) {
                    if (!empty($value)) {
                        $meal->addTranslation($locale, $field, $value);
                    }
                }
            }

            if ($request->photos) {
                foreach ($request->photos as $photo) {
                    $loaderService->upload(Meal::class, $meal->id, $photo);
                }
            }
        });

        return $meal->load('prices');
    }

    public function update(MealRequest $request, ImageLoaderService $loaderService): Meal
    {
        DB::transaction(function () use ($request, $loaderService) {
            $this->model->update([
                'category_id' => $request->category_id,
                'youtube' => $request->youtube ?? null,
            ]);

            $prices = $this->model->prices;
            $existsPrices = $prices->pluck('id');
            $newPrices = array_column($request->prices, 'id');
            foreach ($existsPrices as $existsPrice) {
                if (!in_array($existsPrice, $newPrices)) {
                    $prices->find($existsPrice)->delete();
                }
            }
            foreach ($request->prices as $price) {
                if (isset($price['id'])) {
                    $prices->find($price['id'])->update($price);
                } else {
                    $this->model->prices()->create($price);
                }
            }

            foreach ($request->locales as $locale => $data) {
                foreach ($data as $field => $value) {
                    if (!empty($value)) {
                        $this->model->addTranslation($locale, $field, $value);
                    } else {
                        $this->model->deleteTranslation($locale, $field);
                    }
                }
            }

            if ($request->photos) {
                foreach ($request->photos as $photo) {
                    $loaderService->upload(Meal::class, $this->model->id, $photo);
                }
            }

            if ($request->delete_photos) {
                $oldResources = $this->model->resources;
                foreach ($oldResources as $oldResource) {
                    if (in_array($oldResource->id, $request->delete_photos)) {
                        $loaderService->unlink($oldResource);
                    }
                }
            }
        });

        return $this->model->load('prices');
    }

    /**
     * @throws \App\Exceptions\ApiError
     */
    public function delete(ImageLoaderService $loaderService): ?bool
    {
        $this->model->prices()->delete();
        $this->model->deleteTranslation();
        $resources = $this->model->resources;
        if ($resources->count()) {
            foreach ($resources as $resource) {
                $loaderService->unlink($resource);
            }
        }

        return $this->model->delete();
    }
}
