<?php

namespace App\Policies;

use App\Models\Restaurant;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RestaurantPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function viewAny(): bool
    {
        return true;
    }

    public function view(): bool
    {
        return true;
    }

    public function create(): bool
    {
        return false;
    }

    public function delete(): bool
    {
        return false;
    }

    /**
     * @param \App\Models\User $user
     * @param \App\Models\Restaurant $restaurant
     * @return bool
     */
    public function update(User $user, Restaurant $restaurant): bool
    {
        return $restaurant->user_id === $user->id;
    }
}
