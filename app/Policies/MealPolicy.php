<?php

namespace App\Policies;

use App\Dictionaries\RoleDictionary;
use App\Models\Meal;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MealPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Meal $model
     * @return mixed
     */
    public function view(User $user, Meal $model)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole(RoleDictionary::OWNER);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Meal $model
     * @return mixed
     */
    public function update(User $user, Meal $model)
    {
        return $user->id === $model->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Meal $model
     * @return mixed
     */
    public function delete(User $user, Meal $model)
    {
        return $user->id === $model->user_id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User $user
     * @param Meal $model
     * @return mixed
     */
    public function restore(User $user, Meal $model)
    {
        return $user->hasRole(RoleDictionary::SUPER_ADMIN);
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User $user
     * @param Meal $model
     * @return mixed
     */
    public function forceDelete(User $user, Meal $model)
    {
        return $user->hasRole(RoleDictionary::SUPER_ADMIN);
    }
}
