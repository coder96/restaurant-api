<?php

namespace App\Exceptions;

class ForbiddenException extends \Exception implements UserExceptionInterface
{
    protected $message = 'Forbidden';
    protected $code = 403;
}
