<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class ApiError extends Exception
{
    private string $apiMessage;

    public function __construct($message = "", $code = 500, Throwable $exception = null)
    {
        $this->apiMessage = $message;
        parent::__construct($exception ? $exception->getMessage() : '', $code, $exception);
    }

    public function getApiErrorMessage(): string
    {
        return $this->apiMessage;
    }
}
