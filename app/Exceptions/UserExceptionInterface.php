<?php

namespace App\Exceptions;

interface UserExceptionInterface
{
    public function getMessage();

    public function getCode();
}
