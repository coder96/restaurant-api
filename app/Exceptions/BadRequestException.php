<?php

namespace App\Exceptions;

class BadRequestException extends \Exception implements UserExceptionInterface
{
    protected $message = 'Invalid request';
    protected $code = 422;
}
