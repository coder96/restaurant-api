<?php

namespace App\Exceptions;

class NotFoundException extends \Exception implements UserExceptionInterface
{
    protected $message = 'Объект не найден';
    protected $code = 404;
}
