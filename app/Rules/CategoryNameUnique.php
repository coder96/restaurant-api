<?php

namespace App\Rules;

use App\Models\Category;
use App\Services\MyRestaurantService;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Query\JoinClause;

class CategoryNameUnique implements Rule
{
    private int $categoryId;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(int $categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $locale = explode('.', $attribute)[1];

        $query = Category::query()
            ->join('translations', function (JoinClause $join) use ($locale) {
                $join->on('categories.id', 'translations.model_id')
                    ->where('translations.model', Category::class)
                    ->where('translations.locale', $locale);
            })
            ->where('categories.id', '<>', $this->categoryId)
            ->where('translations.value', '=', $value)
            ->where('categories.restaurant_id', app(MyRestaurantService::class)->restaurant()->id);

        return !$query->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This category name is already exists';
    }
}
