<?php

namespace App\Models;

use App\Traits\HasTranslation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Category
 */
class Category extends Model
{
    use HasFactory;
    use HasTranslation;

    protected $translatableFields = [
        'name',
    ];

    protected $fillable = [
        'icon',
        'restaurant_id',
    ];

    public function meals(): HasMany
    {
        return $this->hasMany(Meal::class)
            ->orderBy('position');
    }

    public function activeMeals(): HasMany
    {
        return $this->meals()->active();
    }

    public function restaurant(): BelongsTo
    {
        return $this->belongsTo(Restaurant::class);
    }
}
