<?php

namespace App\Models;

use App\Traits\HasResources;
use App\Traits\HasTranslation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Meal
 */
class Meal extends Model
{
    use HasFactory;
    use HasResources;
    use HasTranslation;

    protected array $translatableFields = [
        'name',
        'description',
    ];

    protected $fillable = [
        'user_id',
        'category_id',
        'youtube',
        'photo',
        'is_active',
        'position',
    ];

    protected $with = ['prices', 'category', 'resources', 'locales'];

    /**
     * Scope a query to only include for a user.
     *
     * @param Builder $query
     * @param int $userId
     * @return Builder
     */
    public function scopeByUser(Builder $query, int $userId): Builder
    {
        return $query->where('user_id', $userId);
    }

    /**
     * Scope a query to only include active items.
     *
     * @param Builder $query
     * @param int $userId
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('is_active', 1);
    }

    public function prices(): HasMany
    {
        return $this->hasMany(MealPrice::class);
    }

    public function mealPrice(): HasMany
    {
        return $this->prices();
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
