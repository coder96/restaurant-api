<?php

namespace App\Models;

use App\Traits\HasResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Restaurant
 */
class Restaurant extends Model
{
    use HasFactory;
    use HasResource;

    protected $fillable = [
        'user_id',
        'name',
        'description',
        'address',
        'phone',
        'social_networks',
        'slug',
        'working_days',
        'working_time',
    ];

    protected $with = ['resource'];

    public static $rules = [
        'name' => 'required|string',
        'description' => 'string',
        'address' => 'required|string',
        'phone' => 'required|string',
        'social_networks' => 'array',
        'working_days' => 'string',
        'working_time' => 'string',
    ];

    protected $casts = [
        'social_networks' => 'json',
        'address' => 'string',
        'description' => 'string',
    ];

    /**
     * Scope a query to only include active users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return void
     */
    public function scopeActive($query)
    {
        $query->where('active', 1);
    }

    /**
     * Scope a query to only include slug.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return void
     */
    public function scopeSlug($query, string $slug)
    {
        $query->where('slug', $slug);
    }

    public function categories(): HasMany
    {
        return $this->hasMany(Category::class)
            ->orderBy('position');
    }
}
