<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Storage;

class Resource extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'model_type',
        'model_id',
        'name',
        'extension',
        'path',
    ];

    /**
     * Get resource url.
     *
     * @param string $type [thumb]
     * @return string
     */
    public function url(string $type = ''): string
    {
        return Storage::url($this->getPath($type));
    }

    /**
     * Get resource thumbnail url.
     *
     * @return string
     */
    public function thumbnailUrl(): string
    {
        return $this->url('_thumb');
    }

    /**
     * Get file as blob.
     *
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function photoBlob(): string
    {
        $extension = $this->extension === 'jpg' ? 'jpeg' : $this->extension;
        return 'data:image/' . $extension . ';base64,' . base64_encode(Storage::get($this->getPath()));
    }

    /**
     * Get file path.
     *
     * @param string $type
     * @return string
     */
    private function getPath(string $type = ''): string
    {
        return $this->path . $this->name . $type . '.' . $this->extension;
    }
}
