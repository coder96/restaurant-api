<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MealPrice
 */
class MealPrice extends Model
{
    use HasFactory;

    protected $casts = [
        'quantity' => 'float',
        'price' => 'float',
    ];

    protected $fillable = [
        'meal_id',
        'quantity',
        'measure',
        'price',
    ];
}
