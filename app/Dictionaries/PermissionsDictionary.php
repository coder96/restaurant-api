<?php

namespace App\Dictionaries;

class PermissionsDictionary
{
    public const CREATE_FOOD = 'create_food';
    public const CREATE_CATEGORY = 'create_category';
}
