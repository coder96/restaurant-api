<?php

namespace App\Dictionaries;

class RoleDictionary
{
    public const SUPER_ADMIN = 'SA';
    public const ADMIN = 'admin';
    public const OWNER = 'owner';
}
