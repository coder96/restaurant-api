<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string $name
 * @property int $category_id
 * @property string $description
 * @property string $youtube
 * @property array $prices
 */
class MealRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category_id' => 'required|exists:categories,id',
            'description' => 'nullable|string|min:15',
            'position' => 'int|min:0',
            'prices.*.quantity' => 'required|numeric',
            'prices.*.measure' => 'string|in:gram,pieces,portion,ml,liter',
            'prices.*.price' => 'required|numeric',
            'locales' => 'required|array',
            'locales.ru.name' => 'required',
            'locales.*.name' => 'nullable|string|min:2',
            'locales.*.description' => 'nullable|string|min:15',
            'photos' => 'nullable|array|max:10',
            'photos.*' => 'string',
            'youtube' => 'nullable|string',
            'delete_photos' => 'nullable|array',
        ];
    }
}
