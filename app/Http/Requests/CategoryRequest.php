<?php

namespace App\Http\Requests;

use App\Rules\CategoryNameUnique;
use Illuminate\Foundation\Http\FormRequest;


/**
 * @property $name string
 * @property $icon string
 */
class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'locales' => 'required|array',
            'locales.ru.name' => 'required',
            'locales.*.name' => [
                'nullable',
                'string',
                new CategoryNameUnique($this->id ?? 0),
            ],
            'icon' => 'required|string',
        ];
    }
}
