<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property-read $name
 * @property-read $email
 * @property-read $password
 * @property-read $phone
 * @property-read $restaurant_name
 * @property-read $restaurant_slug
 */
class UserPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:5',
            'email' => 'required|email|min:5|unique:users',
            'password' => 'required|string|min:8',
            'phone' => 'required|string|regex:/[\d\s()\+\-]+/',
            'restaurant_name' => 'required|string|min:5|unique:restaurants,name',
            'restaurant_slug' => 'required|string|min:5|unique:restaurants,slug',
        ];
    }
}
