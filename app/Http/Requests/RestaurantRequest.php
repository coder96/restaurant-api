<?php

namespace App\Http\Requests;

use App\Models\Restaurant;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property Restaurant $restaurant
 */
class RestaurantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = Restaurant::$rules;

        $rules['slug'] = 'required|regex:/^[\w\-\_\.]+$/|unique:restaurants,slug';

        if ($this->restaurant instanceof Restaurant) {
            $rules['slug'] .= ',' . $this->restaurant->id;
        }

        return $rules;
    }
}
