<?php

namespace App\Http\Resources;

use App\Models\Category;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;

class CategoryResource extends JsonResource
{
    /**
     * @inerhitDoc
     */
    public $resource = Category::class;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $meals = $this->whenLoaded('meals');
        if ($meals instanceof MissingValue) {
            $meals = $this->whenLoaded('activeMeals');
        }

        return [
            'id' => $this->id,
            'locales' => $this->getLocales(),
            'icon' => $this->icon,
            'meals' => MealResource::collection($meals),
        ];
    }
}
