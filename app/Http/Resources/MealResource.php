<?php

namespace App\Http\Resources;

use App\Models\Meal;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * MealResource.
 *
 * @property \Illuminate\Database\Eloquent\Collection $locales
 */
class MealResource extends JsonResource
{
    /**
     * @inerhitDoc
     */
    public $resource = Meal::class;

    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'photos' => $this->photos,
            'youtube' => $this->youtube,
            'is_active' => $this->is_active,
            'prices' => MealPriceResource::collection($this->prices),
            'category' => $this->category->name,
            'category_id' => $this->category->id,
            'locales' => $this->getLocales(),
        ];
    }
}
