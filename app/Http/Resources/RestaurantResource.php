<?php

namespace App\Http\Resources;

use App\Models\Restaurant;
use Illuminate\Http\Resources\Json\JsonResource;

class RestaurantResource extends JsonResource
{
    /**
     * Convert image url to blob.
     *
     * @var bool
     */
    private bool $imageToBlob;

    public function __construct($resource, bool $imageToBlob = false)
    {
        parent::__construct($resource);
        $this->imageToBlob = $imageToBlob;
    }

    /**
     * @inerhitDoc
     */
    public $resource = Restaurant::class;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var $this \App\Models\Restaurant|$this
         */
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'address' => $this->address,
            'phone' => $this->phone,
            'social_networks' => $this->social_networks,
            'slug' => $this->slug,
            'photo' => $this->imageToBlob ? $this->photo_blob : $this->photo,
            'photo_thumbnail' => $this->thumbnail,
            'working_days' => $this->working_days,
            'working_time' => $this->working_time,
            'categories' => CategoryResource::collection($this->whenLoaded('categories')),
        ];
    }
}
