<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Http\Requests\Positions\CategoryPositionRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Services\MyRestaurantService;
use App\Services\Positions\CategoryPositionService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return CategoryResource::collection((new MyRestaurantService())->categories());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\CategoryResource
     */
    public function store(CategoryRequest $request, MyRestaurantService $service): CategoryResource
    {
        return new CategoryResource($service->addCategory($request->toArray()));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Category $category
     * @return \App\Http\Resources\CategoryResource
     */
    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Category $category
     * @return \App\Http\Resources\CategoryResource
     */
    public function update(CategoryRequest $request, Category $category, MyRestaurantService $service): CategoryResource
    {
        $service->updateCategory($request->toArray(), $category);

        return new CategoryResource($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Category $category
     * @return false
     */
    public function destroy(Category $category)
    {
        return false;
    }

    public function positions(CategoryPositionRequest $request, CategoryPositionService $service)
    {
        $service->updatePositions($request->data);

        return ['result'=> true];
    }
}
