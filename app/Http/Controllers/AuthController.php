<?php

namespace App\Http\Controllers;

use App\Exceptions\BadRequestException;
use App\Http\Requests\UserLoginCredentials;
use App\Http\Requests\UserRegisterCredentials;
use App\Http\Resources\UserResource;
use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(UserRegisterCredentials $request)
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        return $this->getAccessTokens($request->email, $request->password);
    }

    public function login(UserLoginCredentials $request)
    {
        if (auth()->attempt($request->all())) {
            $response = $this->getAccessTokens($request->email, $request->password);

            $user = auth()->user();
            $user->load('roles');
            $response['user'] = new UserResource($user);

            return $response;
        }

        throw new BadRequestException('Invalid credentials');
    }

    private function getAccessTokens(string $username, string $password)
    {
        $user =  User::where('email', $username)->first();

        if(!Hash::check($password, $user->password, [])){
            return response()->json([
                'status_code' => 422,
                'message' => 'Password Match',
            ]);
        }

        return [
            'access_token' => $user->createToken('authToken')->plainTextToken,
        ];
    }

    public function authenticatedUserDetails()
    {
        //returns details
        $user = auth()->user();
        $user->load('roles');

        return ['user' => new UserResource($user)];
    }
}
