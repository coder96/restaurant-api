<?php

namespace App\Http\Controllers;

use App\Components\MealComponent;
use App\Http\Requests\MealRequest;
use App\Http\Requests\Positions\MealPositionRequest;
use App\Http\Resources\MealResource;
use App\Models\Meal;
use App\Services\ImageLoaderService;
use App\Services\MealService;
use App\Services\MyRestaurantService;
use App\Services\Positions\MealPositionService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class MealController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Meal::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $mealsGateway = (new MyRestaurantService())->meals();
        if ($request->category_id) {
            $mealsGateway->where('category_id', $request->category_id);
        }
        return MealResource::collection($mealsGateway->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\MealRequest $request
     * @return \App\Http\Resources\MealResource
     */
    public function store(MealRequest $request, ImageLoaderService $loaderService): MealResource
    {
        $component = new MealComponent(new Meal());

        return new MealResource(
            $component->store($request, auth()->user()->id, $loaderService)
        );
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Meal $meal
     * @return \App\Http\Resources\MealResource
     */
    public function show(Meal $meal)
    {
        return new MealResource($meal);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\MealRequest $request
     * @param \App\Models\Meal $meal
     * @throws \App\Exceptions\ForbiddenException
     * @return \App\Http\Resources\MealResource
     */
    public function update(MealRequest $request, Meal $meal, ImageLoaderService $loaderService): MealResource
    {
        $component = new MealComponent($meal);

        return new MealResource(
            $component->update($request, $loaderService)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Meal $meal
     * @param ImageLoaderService $loaderService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \App\Exceptions\ApiError
     */
    public function destroy(Meal $meal, ImageLoaderService $loaderService)
    {
        $component = new MealComponent($meal);
        $res = $component->delete($loaderService);
        return $res ? response(null, 204) : response([
            'error' => 'Error while deleting object'
        ], 520);
    }

    /**
     * @param Meal $meal
     * @param Request $request
     * @param MealService $service
     *
     * @return string[]
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function activity(Meal $meal, Request $request, MealService $service): array
    {
        $this->authorize('update', $meal);
        if ($request->state) {
            $service->activate($meal);
        } else {
            $service->deactivate($meal);
        }

        return ['res' => 'ok'];
    }

    public function positions(MealPositionRequest $request, MyRestaurantService $service)
    {
        $service = new MealPositionService($service, $request->category_id);
        $service->updatePositions($request->data);
        return ['res'=> 'ok'];
    }
}
