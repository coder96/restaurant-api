<?php

namespace App\Http\Controllers;

use App\Http\Requests\RestaurantRequest;
use App\Http\Resources\RestaurantResource;
use App\Models\Restaurant;
use App\Repositories\RestaurantRepository;
use App\Services\ImageLoaderService;
use App\Services\MyRestaurantService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    public function __construct(Request $request)
    {
        if ($request->method() !== 'GET') {
            $this->authorizeResource(Restaurant::class);
        }
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        return RestaurantResource::collection(Restaurant::active()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\RestaurantResource
     */
    public function store(RestaurantRequest $request, RestaurantRepository $repository): RestaurantResource
    {
        return new RestaurantResource($repository->create($request));
    }

    /**
     * Display the specified resource.
     *
     * @param string $slug
     * @return RestaurantResource
     */
    public function show(string $slug): RestaurantResource
    {
        $model = Restaurant::with(['categories.activeMeals']);
        if (is_numeric($slug)) {
            $restaurant = $model->findOrFail($slug);
        } else {
            $restaurant = $model->slug($slug)->first();
            if (!$restaurant) {
                throw new ModelNotFoundException();
            }
        }

        return new RestaurantResource($restaurant);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\RestaurantRequest $request
     * @param \App\Models\Restaurant $restaurant
     * @return \App\Http\Resources\RestaurantResource
     */
    public function update(RestaurantRequest $request, Restaurant $restaurant, MyRestaurantService $service, ImageLoaderService $imageLoaderService): RestaurantResource
    {
        $restaurant = $service->update($restaurant, $request, $imageLoaderService);

        return new RestaurantResource($restaurant);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Restaurant $restaurant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Restaurant $restaurant): \Illuminate\Http\Response
    {
        $res = $restaurant->delete();
        return $res ? response(null, 204) : response([
            'error' => 'Error while deleting object'
        ], 520);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Restaurant $restaurant
     * @return \App\Http\Resources\RestaurantResource
     */
    public function myRestaurant(): RestaurantResource
    {
        return new RestaurantResource(auth()->user()->restaurant()->first(), true);
    }
}
