<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MealController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RestaurantController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('locales', [Controller::class, 'locales']);
Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::resource('restaurants', RestaurantController::class)
    ->only(['index', 'show']);

Route::get('meals/{meal}', [MealController::class, 'show']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('user', [AuthController::class, 'authenticatedUserDetails']);

    Route::resource('users', UserController::class);
    Route::resource('meals', MealController::class)
        ->except(['show']);
    Route::resource('categories', CategoryController::class);
    Route::resource('restaurants', RestaurantController::class)
        ->except(['show', 'index']);
    Route::get('restaurant', [RestaurantController::class, 'myRestaurant']);

    Route::post('meals/positions', [MealController::class, 'positions']);
    Route::put('meals/{meal}/activity', [MealController::class, 'activity']);

    Route::post('categories/positions', [CategoryController::class, 'positions']);
});

//Route::get('restaurants', [RestaurantController::class, 'index']);
